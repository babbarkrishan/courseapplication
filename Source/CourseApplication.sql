-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.18-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for courseapp
DROP DATABASE IF EXISTS `courseapp`;
CREATE DATABASE IF NOT EXISTS `courseapp` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `courseapp`;

-- Dumping structure for table courseapp.course
DROP TABLE IF EXISTS `course`;
CREATE TABLE IF NOT EXISTS `course` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `courseName` varchar(200) NOT NULL DEFAULT '0',
  `duration` varchar(200) NOT NULL DEFAULT '0',
  `description` varchar(500) DEFAULT '0',
  `eligibility` varchar(200) DEFAULT '0',
  `universityId` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- Dumping data for table courseapp.course: ~11 rows (approximately)
DELETE FROM `course`;
/*!40000 ALTER TABLE `course` DISABLE KEYS */;
INSERT INTO `course` (`id`, `courseName`, `duration`, `description`, `eligibility`, `universityId`) VALUES
	(3, 'MCA', '2 Years', 'MCA is Master of Computer Applications', 'Graduation', 5),
	(4, 'MCA', '2 Years', 'MCA is Master of Computer Applications', 'Graduation', 6),
	(5, 'M.SC.', '2 Years', 'M.SC. is Master of Computer Science ant it is of 2 years programme', 'Graduation', 6),
	(6, 'M.SC.', '2 Years', 'M.SC. is Master of Computer Science', 'Graduation', 6),
	(7, 'M.SC.', '2 Years', 'M.SC. is Master of Computer Science', 'Graduation', 6),
	(8, 'M.SC.', '2 Years', 'M.SC. is Master of Computer Science', 'Graduation', 6),
	(9, 'M.SC.', '2 Years', 'M.SC. is Master of Computer Science', 'Graduation', 6),
	(10, 'M.SC.', '2 Years', 'M.SC. is Master of Computer Science', 'Graduation', 6),
	(11, 'M.SC.', '2 Years', 'M.SC. is Master of Computer Science', 'Graduation', 6),
	(12, 'M.SC.', '3 Years', 'M.SC. is Master of Computer Science', 'Graduation', 6),
	(13, 'M.SC.', '3 Years', 'M.SC. is Master of Computer Science', 'Graduation', 6);
/*!40000 ALTER TABLE `course` ENABLE KEYS */;

-- Dumping structure for table courseapp.student
DROP TABLE IF EXISTS `student`;
CREATE TABLE IF NOT EXISTS `student` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstName` varchar(50) NOT NULL,
  `lastName` varchar(50) NOT NULL,
  `fatherName` varchar(100) NOT NULL,
  `gender` varchar(6) NOT NULL,
  `age` int(11) NOT NULL,
  `address` varchar(200) DEFAULT NULL,
  `phoneNumber` varchar(15) NOT NULL,
  `courseId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table courseapp.student: ~0 rows (approximately)
DELETE FROM `student`;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` (`id`, `firstName`, `lastName`, `fatherName`, `gender`, `age`, `address`, `phoneNumber`, `courseId`) VALUES
	(1, 'Kulwant', 'Singh', 'Jaswinder Singh', 'Male', 22, '62-A, lssksk, near kskd, kaha, Mohali. Punjab 140587', '1234566541', 5);
/*!40000 ALTER TABLE `student` ENABLE KEYS */;

-- Dumping structure for table courseapp.university
DROP TABLE IF EXISTS `university`;
CREATE TABLE IF NOT EXISTS `university` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `universityName` varchar(200) NOT NULL,
  `address` varchar(300) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phoneNumber` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- Dumping data for table courseapp.university: ~12 rows (approximately)
DELETE FROM `university`;
/*!40000 ALTER TABLE `university` DISABLE KEYS */;
INSERT INTO `university` (`id`, `universityName`, `address`, `email`, `phoneNumber`) VALUES
	(1, 'Kurukshetra University', 'Near Brahm Sarovar, Kurukshetra', 'info@kuk.com', '3214569879'),
	(2, 'Panjab University', 'Sector 15, Chandigarh', 'info@panjabuniversity.com', '1234567895'),
	(3, 'PTU University', 'Jalandhar', 'info@ptu.com', '2726263636'),
	(4, 'ksd', 'sda', 'sdf@dfa.sdfa', '1234554321'),
	(5, 'ksd89', 'sda', 'sdf@dfa.sdfa', '1234554321'),
	(6, 'ksd7790', 'sda', 'sdf@dfa.sdfa', '1234554321'),
	(7, 'ksd5', 'sda', 'sdf@dfa.sdfa', '1234554321'),
	(8, 'ksd8', 'sda', 'sdf@dfa.sdfa', '1234554321'),
	(9, 'ksd859', 'sda', 'sdf@dfa.sdfa', '1234554321'),
	(10, 'ksd8595', 'sda', 'sdf@dfa.sdfa', '1234554321'),
	(11, 'ksd7777', 'sda', 'sdf@dfa.sdfa', '1234554321'),
	(12, 'sadf', 'da', 'E@dsfa.s', '5558889977');
/*!40000 ALTER TABLE `university` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

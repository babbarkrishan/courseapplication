# README #

This project will have following features.
- User Interface with the following functionalities
	- List Universities, Courses, Enrolled Students
	- Facility to Add / Remove Course/Students
	- Course Registration for a Student
	- Change course/Unregister feature for any student
	- Ignore Authorization	

### What is this repository for? ###

* This application is a demo project made in Spring 4, Hibernate 4, AngularJs 1.4.4 and MySQL 5. It would have features mentioned above.
* 1.0.0


### How do I get set up? ###

* Clone "CourseApplication" repository at your local machine
* Execute "Source/CourseApplication.sql" in MySQL to create DB named "courseapp" and required tables.
* Change DB credentials in "CourseAppConfiguration.java" file. These will move to config file later.
* Import "CourseApplication" project as Maven Existing project in your Eclipse. I am using "Spring Tool Suite" Version: 3.8.4.RELEASE.
* Use maven and execute Clean and Install commands.
* Run your project on server. I am using apache-tomcat-8.0.30.

### Contribution guidelines ###

* We are using Spring MVC, Hibernate, AngularJs and MySQL.
* You may add your changes as per your requirements but try not to change existing technologies unless you have a solid reason.

### Explanation of what could be done with more time ###

As per my understanding, I can add following features if get more time.

- Responsive Design.
- Add more Validations on both client side and server side.
- Cache can be added for better performance for Searching. We can use EhCache or Hazelcast depending on environment.
- Multilingual support can be added, which may require changes in UI, Service layer and in DB.
- On list pages we can add sorting, filters, search by keywords like we have in Amazon.in
- UI can be more attactive and simple with some logo, navigation, header and footer.
- Add logging.
- Move DB credentials in a config file.
- Add Test Cases.
- Use Jenkins for continue deployments and link the same with BitBucket.

### Project builds / runs / tests as per instruction ###
- I have added a "CourseApplication.war" in targets directory, which you may run after setting up DB.

### Who do I talk to? ###

* You may reach to me at krishan.babbar@gmail.com
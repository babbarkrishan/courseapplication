package com.babbarkrishan.university.bean;

public class CourseSearchCriteria {
	Long universityId;

	public Long getUniversityId() {
		return universityId;
	}

	public void setUniversityId(Long universityId) {
		this.universityId = universityId;
	}
}

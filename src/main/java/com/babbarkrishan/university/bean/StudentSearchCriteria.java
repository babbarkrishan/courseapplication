package com.babbarkrishan.university.bean;

public class StudentSearchCriteria {
	Long courseId;

	public Long getCourseId() {
		return courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}
}

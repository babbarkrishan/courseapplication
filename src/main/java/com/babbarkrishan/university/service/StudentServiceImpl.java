package com.babbarkrishan.university.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.babbarkrishan.university.bean.StudentSearchCriteria;
import com.babbarkrishan.university.bean.StudentSearchResultRow;
import com.babbarkrishan.university.bean.UIStudent;
import com.babbarkrishan.university.dao.CourseDAO;
import com.babbarkrishan.university.dao.StudentDAO;
import com.babbarkrishan.university.entity.Student;
import com.babbarkrishan.university.exception.NotFoundException;

@Service("studentService")
public class StudentServiceImpl implements StudentService {
	
	private static final Logger logger = LoggerFactory.getLogger(StudentServiceImpl.class);
	
	@Autowired
    private StudentDAO dao;	

	@Autowired
    private CourseDAO studentDao;

	@Override
	@Transactional
	public Set<StudentSearchResultRow> findAllStudents() {
		return this.searchStudent(null);	
	}
	
	public Set<StudentSearchResultRow> searchStudent(StudentSearchCriteria studentSearchCriteria) {
		List<Student> students = null;
		
		if (studentSearchCriteria == null) {
			students = dao.list(null);
		}
		else {
			students = dao.list(studentSearchCriteria.getCourseId());
		}
		
		List<Long> uniqueStudentIds = new ArrayList<Long>(students.size());
		Set<StudentSearchResultRow> studentsResult = new HashSet<StudentSearchResultRow>(students.size());
		
		for(Student student: students) {
			if(uniqueStudentIds.contains(student.getId())) {
				continue;
			}
			else {
				uniqueStudentIds.add(student.getId());
			}
			
			studentsResult.add(populateStudentSearchRow(student));
		}
		return studentsResult;
	}
	
	private StudentSearchResultRow populateStudentSearchRow(Student student) {
		StudentSearchResultRow studentSearchResultRow = new StudentSearchResultRow();
		studentSearchResultRow.setStudentId(student.getId());
		studentSearchResultRow.setFirstName(student.getFirstName());
        studentSearchResultRow.setLastName(student.getLastName());
        studentSearchResultRow.setFatherName(student.getFatherName());
        studentSearchResultRow.setGender(student.getGender());
        studentSearchResultRow.setAge(student.getAge());
        studentSearchResultRow.setAddress(student.getAddress());
        studentSearchResultRow.setPhoneNumber(student.getPhoneNumber());
        if (student.getCourse() != null) {
        	studentSearchResultRow.setCourseId(student.getCourse().getId());
        	studentSearchResultRow.setCourseName(student.getCourse().getCourseName());
        }       
       
		return studentSearchResultRow;
	}
	
	@Override
	@Transactional
	public StudentSearchResultRow findById(Long id) {
		Student student = dao.get(id);
		if(student != null) {
			return populateStudentSearchRow(student);
		}
		return null;
	}
	
	@Transactional
	public Student saveStudent(UIStudent uiStudent) {
		Student student = new Student();
		student = this.populateStudentEntityFromUIStudent(uiStudent, student);
		dao.save(student);
		return student;
	}

	@Transactional
	public Student updateStudent(UIStudent uiStudent, Long id)  throws NotFoundException {
		
		Student currentStudent = dao.get(id);
		
        if (currentStudent==null) {
        	logger.debug("Student with id {} not found", id);
        	throw new NotFoundException();
        }
        currentStudent = this.populateStudentEntityFromUIStudent(uiStudent, currentStudent);
               
        dao.update(currentStudent);
		return currentStudent;
	}

	@Override
	@Transactional
	public Set<StudentSearchResultRow> searchStudents(StudentSearchCriteria studentSearchCriteria) {
		return this.searchStudent(studentSearchCriteria);	
	}
	
	@Transactional
    private Student populateStudentEntityFromUIStudent(UIStudent uiStudent, Student student) { 
    	// TODO: Add server side validation
		
		student.setFirstName(uiStudent.getFirstName());
        student.setLastName(uiStudent.getLastName());
        student.setFatherName(uiStudent.getFatherName());
        student.setGender(uiStudent.getGender());
        student.setAge(uiStudent.getAge());
        student.setAddress(uiStudent.getAddress());
        student.setPhoneNumber(uiStudent.getPhoneNumber());
        if (uiStudent.getCourseId() != null && uiStudent.getCourseId() > 0) {
 			student.setCourse(studentDao.get(uiStudent.getCourseId()));
		}
		return student;
	}
}
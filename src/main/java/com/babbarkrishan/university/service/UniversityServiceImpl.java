package com.babbarkrishan.university.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.babbarkrishan.university.bean.UIUniversity;
import com.babbarkrishan.university.bean.UniversitySearchResultRow;
import com.babbarkrishan.university.dao.UniversityDAO;
import com.babbarkrishan.university.entity.University;
import com.babbarkrishan.university.exception.NotFoundException;

@Service("universityService")
public class UniversityServiceImpl implements UniversityService{

	private static final Logger logger = LoggerFactory.getLogger(UniversityServiceImpl.class);
	
	@Autowired
    private UniversityDAO dao;
	
	@Transactional
	public Set<UniversitySearchResultRow> findAllUniversities() {
		List<University> universities = dao.list();
		
		List<Long> uniqueUniversityIds = new ArrayList<Long>(universities.size());
		Set<UniversitySearchResultRow> universitiesResult = new HashSet<UniversitySearchResultRow>(universities.size());
		
		for(University university: universities) {
			if(uniqueUniversityIds.contains(university.getId())) {
				continue;
			}
			else {
				uniqueUniversityIds.add(university.getId());
			}
			
			universitiesResult.add(populateUniversitySearchRow(university));
		}
		return universitiesResult;
	}
	
	private UniversitySearchResultRow populateUniversitySearchRow(University university) {
		UniversitySearchResultRow universitySearchResultRow = new UniversitySearchResultRow();
		universitySearchResultRow.setUniversityId(university.getId());
		universitySearchResultRow.setUniversityName(university.getUniversityName());
		universitySearchResultRow.setAddress(university.getAddress());
		universitySearchResultRow.setEmail(university.getEmail());
		universitySearchResultRow.setPhoneNumber(university.getPhoneNumber());
		universitySearchResultRow.setCourseCount(university.getCourses().size());
		return universitySearchResultRow;
	}

	@Transactional
	public UniversitySearchResultRow findById(Long id) {
		University university = dao.get(id);
		
		if(university != null) {
			return populateUniversitySearchRow(university);
		}
		return null;
	}
	
	@Transactional
	public University findByName(String name) {
		University university = dao.getByName(name);
		if(university != null) {
				return university;
		}
		return null;
	}
	
	@Transactional
	public University saveUniversity(UIUniversity uiUniversity) {
		University university = populateUniversity(uiUniversity);
		dao.save(university);
		return university;
	}

	@Transactional
	public University updateUniversity(UIUniversity uiUniversity, Long id) throws NotFoundException{
		
		University currentUniversity = dao.get(id);
        
        if (currentUniversity==null) {
        	logger.debug("University with id {} not found", id);
            throw new NotFoundException();
        }
 
        currentUniversity.setUniversityName(uiUniversity.getUniversityName());
        currentUniversity.setAddress(uiUniversity.getAddress());
        currentUniversity.setEmail(uiUniversity.getEmail());
        currentUniversity.setPhoneNumber(uiUniversity.getPhoneNumber());
 
		dao.update(currentUniversity);
		return currentUniversity;
	}
	
	@Transactional
	public boolean isUniversityExist(UIUniversity uiUniversity) {
		return findByName(uiUniversity.getUniversityName())!=null;
	}	
    
    
   private University populateUniversity(UIUniversity uiUniversity) { 
   		// TODO: Add server side validation
   		University university = new University();
		university.setUniversityName(uiUniversity.getUniversityName());
		university.setAddress(uiUniversity.getAddress());
		university.setEmail(uiUniversity.getEmail());
		university.setPhoneNumber(uiUniversity.getPhoneNumber());
		return university;
	}

}
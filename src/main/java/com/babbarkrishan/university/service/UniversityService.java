package com.babbarkrishan.university.service;

import java.util.Set;

import com.babbarkrishan.university.bean.UIUniversity;
import com.babbarkrishan.university.bean.UniversitySearchResultRow;
import com.babbarkrishan.university.entity.University;
import com.babbarkrishan.university.exception.NotFoundException;



public interface UniversityService {
	
	UniversitySearchResultRow findById(Long id);
	
	University findByName(String name);
	
	University saveUniversity(UIUniversity uiUniversity);
	
	University updateUniversity(UIUniversity uiUniversity, Long id) throws NotFoundException;
	
	Set<UniversitySearchResultRow> findAllUniversities(); 

	public boolean isUniversityExist(UIUniversity uiUniversity);
	
}

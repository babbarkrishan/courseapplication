package com.babbarkrishan.university.service;

import java.util.Set;

import com.babbarkrishan.university.bean.CourseSearchCriteria;
import com.babbarkrishan.university.bean.CourseSearchResultRow;
import com.babbarkrishan.university.bean.UICourse;
import com.babbarkrishan.university.entity.Course;
import com.babbarkrishan.university.exception.NotFoundException;



public interface CourseService {
	
	CourseSearchResultRow findById(Long id);
	
	Course saveCourse(UICourse uiCourse);
	
	Course updateCourse(UICourse uiCourse, Long id)  throws NotFoundException;
	
	Set<CourseSearchResultRow> findAllCourses(); 
	
	Set<CourseSearchResultRow> searchCourses(CourseSearchCriteria courseSearchCriteria);
}

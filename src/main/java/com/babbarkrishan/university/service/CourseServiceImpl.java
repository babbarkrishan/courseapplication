package com.babbarkrishan.university.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.babbarkrishan.university.bean.CourseSearchCriteria;
import com.babbarkrishan.university.bean.CourseSearchResultRow;
import com.babbarkrishan.university.bean.UICourse;
import com.babbarkrishan.university.dao.CourseDAO;
import com.babbarkrishan.university.dao.UniversityDAO;
import com.babbarkrishan.university.entity.Course;
import com.babbarkrishan.university.exception.NotFoundException;

@Service("courseService")
public class CourseServiceImpl implements CourseService {
	
	private static final Logger logger = LoggerFactory.getLogger(CourseServiceImpl.class);
	
	@Autowired
    private CourseDAO dao;	

	@Autowired
    private UniversityDAO universityDao;

	@Override
	@Transactional
	public Set<CourseSearchResultRow> findAllCourses() {
		return this.searchCourse(null);	
	}
	
	public Set<CourseSearchResultRow> searchCourse(CourseSearchCriteria courseSearchCriteria) {
		List<Course> courses = null;
		if (courseSearchCriteria == null) {
			courses = dao.list(null);
		}
		else {
			courses = dao.list(courseSearchCriteria.getUniversityId());
		}
		
		List<Long> uniqueCourseIds = new ArrayList<Long>(courses.size());
		Set<CourseSearchResultRow> coursesResult = new HashSet<CourseSearchResultRow>(courses.size());
		
		for(Course course: courses) {
			if(uniqueCourseIds.contains(course.getId())) {
				continue;
			}
			else {
				uniqueCourseIds.add(course.getId());
			}
			
			coursesResult.add(populateCourseSearchRow(course));
		}
		return coursesResult;
	}
	
	private CourseSearchResultRow populateCourseSearchRow(Course course) {
		CourseSearchResultRow courseSearchResultRow = new CourseSearchResultRow();
		courseSearchResultRow.setCourseId(course.getId());
		courseSearchResultRow.setCourseName(course.getCourseName());
		courseSearchResultRow.setDuration(course.getDuration());
		courseSearchResultRow.setDescription(course.getDescription());
		courseSearchResultRow.setEligibility(course.getEligibility());
		courseSearchResultRow.setUniversityName(course.getUniversity().getUniversityName());
		courseSearchResultRow.setUniversityId(course.getUniversity().getId());
		//courseSearchResultRow.setStudentsCount(course.getEligibility());
		return courseSearchResultRow;
	}
	
	@Override
	@Transactional
	public CourseSearchResultRow findById(Long id) {
		Course course = dao.get(id);
		if(course != null) {
			return populateCourseSearchRow(course);
		}
		return null;
	}
	
	@Transactional
	public Course saveCourse(UICourse uiCourse) {
		Course course = new Course();
		course = this.populateCourseEntityFromUICourse(uiCourse, course);
		dao.save(course);
		return course;
	}

	@Transactional
	public Course updateCourse(UICourse uiCourse, Long id)  throws NotFoundException {
		
		Course currentCourse = dao.get(id);
		
        if (currentCourse==null) {
        	logger.debug("Course with id {} not found", id);
        	throw new NotFoundException();
        }
        currentCourse = this.populateCourseEntityFromUICourse(uiCourse, currentCourse);
               
        dao.update(currentCourse);
		return currentCourse;
	}

	@Override
	@Transactional
	public Set<CourseSearchResultRow> searchCourses(CourseSearchCriteria courseSearchCriteria) {
		return this.searchCourse(courseSearchCriteria);	
	}
	
	@Transactional
    private Course populateCourseEntityFromUICourse(UICourse uiCourse, Course course) { 
    	// TODO: Add server side validation
 		course.setCourseName(uiCourse.getCourseName());
		course.setDuration(uiCourse.getDuration());
		course.setDescription(uiCourse.getDescription());
		course.setEligibility(uiCourse.getEligibility());
		if (uiCourse.getUniversityId() != null && uiCourse.getUniversityId() > 0) {
			course.setUniversity(universityDao.get(uiCourse.getUniversityId()));
		}
		return course;
	}
}
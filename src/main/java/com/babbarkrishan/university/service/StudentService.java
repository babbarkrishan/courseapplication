package com.babbarkrishan.university.service;

import java.util.Set;

import com.babbarkrishan.university.bean.StudentSearchCriteria;
import com.babbarkrishan.university.bean.StudentSearchResultRow;
import com.babbarkrishan.university.bean.UIStudent;
import com.babbarkrishan.university.entity.Student;
import com.babbarkrishan.university.exception.NotFoundException;

public interface StudentService {
	
	StudentSearchResultRow findById(Long id);
	
	Student saveStudent(UIStudent uiStudent);
	
	Student updateStudent(UIStudent uiStudent, Long id)  throws NotFoundException;
	
	Set<StudentSearchResultRow> findAllStudents(); 
	
	Set<StudentSearchResultRow> searchStudents(StudentSearchCriteria courseSearchCriteria);
}

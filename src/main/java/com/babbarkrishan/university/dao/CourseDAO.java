package com.babbarkrishan.university.dao;

import java.util.List;

import com.babbarkrishan.university.entity.Course;
 
public interface CourseDAO {
    public List<Course> list(Long universityId);
     
    public Course get(Long id);
     
    public void save(Course course);     
    public void update(Course course);

	public Course getByName(String name);
}
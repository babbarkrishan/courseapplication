package com.babbarkrishan.university.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.babbarkrishan.university.entity.Student;
 
@Repository
public class StudentDAOImpl extends BaseDAO implements StudentDAO {
	
    @Override
    @Transactional
    public List<Student> list(Long courseId) {
    	StringBuilder hql = new StringBuilder("SELECT student from Student student ");
    	if (courseId != null && courseId > 0) {
    		hql.append(" WHERE student.course.id=?");
    	}
    	Query query = getCurrentSession().createQuery(hql.toString());
    	
    	if (courseId != null && courseId > 0) {
    		query.setParameter(0, courseId);
    	}
    	
    	@SuppressWarnings("unchecked")
		List<Student> listStudent = (List<Student>) query.list();
    	
        return listStudent;
    }
 
    @Override
    public void save(Student student) {
        getCurrentSession().save(student);
    }
    
    @Override
    public void update(Student student) {
        getCurrentSession().merge(student);
    }
  
    @Override
    @Transactional
    public Student get(Long id) {
        String hql = "FROM Student student LEFT JOIN FETCH student.course WHERE student.id=?";
        Query query = getCurrentSession().createQuery(hql);
        query.setParameter(0, id);
        @SuppressWarnings("unchecked")
        List<Student> listStudent = (List<Student>) query.list();
         
        if (listStudent != null && !listStudent.isEmpty()) {
            return listStudent.get(0);
        }
         
        return null;
    }
}
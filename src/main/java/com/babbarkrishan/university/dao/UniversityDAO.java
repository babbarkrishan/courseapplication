package com.babbarkrishan.university.dao;

import java.util.List;

import com.babbarkrishan.university.entity.University;
 
public interface UniversityDAO {
    public List<University> list();
     
    public University get(Long id);
     
    public void save(University university);     
    public void update(University university);

	public University getByName(String name);
}
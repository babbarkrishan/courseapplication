package com.babbarkrishan.university.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.babbarkrishan.university.entity.University;
 
@Repository
public class UniversityDAOImpl extends BaseDAO implements UniversityDAO {
	
    @Override
    public List<University> list() {
    	String hql = "SELECT univ from University univ LEFT JOIN FETCH univ.courses";
    	Query query = getCurrentSession().createQuery(hql);
    	@SuppressWarnings("unchecked")
		List<University> listUniversity = (List<University>) query.list();
    	
        return listUniversity;
    }
 
    @Override
    public void save(University university) {
        getCurrentSession().save(university);
    }
    
    @Override
    public void update(University university) {
        getCurrentSession().merge(university);
    }
  
    @Override
    public University get(Long id) {
        String hql = "SELECT univ FROM University univ LEFT JOIN FETCH univ.courses WHERE univ.id=?";
        Query query = getCurrentSession().createQuery(hql);
        query.setParameter(0, id);
        @SuppressWarnings("unchecked")
        List<University> listUniversity = (List<University>) query.list();
         
        if (listUniversity != null && !listUniversity.isEmpty()) {
            return listUniversity.get(0);
        }
         
        return null;
    }

	@Override
	public University getByName(String name) {
		String hql = "SELECT univ FROM University univ WHERE univ.universityName=?";
        Query query = getCurrentSession().createQuery(hql);
        query.setParameter(0, name);
         
        @SuppressWarnings("unchecked")
        List<University> listUniversity = (List<University>) query.list();
         
        if (listUniversity != null && !listUniversity.isEmpty()) {
            return listUniversity.get(0);
        }
         
        return null;
	}
}
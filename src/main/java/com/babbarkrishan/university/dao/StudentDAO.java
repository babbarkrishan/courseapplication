package com.babbarkrishan.university.dao;

import java.util.List;

import com.babbarkrishan.university.entity.Student;
 
public interface StudentDAO {
    public List<Student> list(Long courseId);
     
    public Student get(Long id);
     
    public void save(Student student);     
    public void update(Student student);
}
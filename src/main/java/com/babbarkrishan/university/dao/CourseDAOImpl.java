package com.babbarkrishan.university.dao;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.babbarkrishan.university.entity.Course;
 
@Repository
public class CourseDAOImpl extends BaseDAO implements CourseDAO {
	
    @Override
    public List<Course> list(Long universityId) {
    	StringBuilder hql = new StringBuilder("SELECT course from Course course ");
    	if (universityId != null && universityId > 0) {
    		hql.append(" WHERE course.university.id=?");
    	}
    	Query query = getCurrentSession().createQuery(hql.toString());
    	
    	if (universityId != null && universityId > 0) {
    		query.setParameter(0, universityId);
    	}
    	
    	@SuppressWarnings("unchecked")
		List<Course> listCourse = (List<Course>) query.list();
    	
        return listCourse;
    }
 
    @Override
    public void save(Course course) {
        getCurrentSession().save(course);
    }
    
    @Override
    public void update(Course course) {
        getCurrentSession().merge(course);
    }
  
    @Override
    public Course get(Long id) {
        String hql = "from Course course LEFT JOIN FETCH course.university where course.id=?";
        Query query = getCurrentSession().createQuery(hql);
        query.setParameter(0, id);
        @SuppressWarnings("unchecked")
        List<Course> listCourse = (List<Course>) query.list();
         
        if (listCourse != null && !listCourse.isEmpty()) {
            return listCourse.get(0);
        }
         
        return null;
    }

	@Override
	public Course getByName(String name) {
		String hql = "from Course where courseName=?";
        Query query = getCurrentSession().createQuery(hql);
        query.setParameter(0, name);
         
        @SuppressWarnings("unchecked")
        List<Course> listCourse = (List<Course>) query.list();
         
        if (listCourse != null && !listCourse.isEmpty()) {
            return listCourse.get(0);
        }
         
        return null;
	}
}
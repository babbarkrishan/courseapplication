package com.babbarkrishan.university.controller;
 
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.babbarkrishan.university.bean.StudentSearchCriteria;
import com.babbarkrishan.university.bean.StudentSearchResultRow;
import com.babbarkrishan.university.bean.UIStudent;
import com.babbarkrishan.university.entity.Student;
import com.babbarkrishan.university.exception.NotFoundException;
import com.babbarkrishan.university.service.StudentService;
 
@RestController
public class StudentRestController {
	
	private static final Logger logger = LoggerFactory.getLogger(StudentRestController.class);
 
    @Autowired
    StudentService studentService; 
    
    //-------------------Retrieve All Students--------------------------------------------------------
     
    @RequestMapping(value = "/students", method = RequestMethod.GET)
    public ResponseEntity<Set<StudentSearchResultRow>> listAllStudents() {
        Set<StudentSearchResultRow> students = studentService.findAllStudents();
        if(students.isEmpty()){
            return new ResponseEntity<Set<StudentSearchResultRow>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Set<StudentSearchResultRow>>(students, HttpStatus.OK);
    } 
    
    @RequestMapping(value = "/students/search", method = RequestMethod.POST)
    public ResponseEntity<Set<StudentSearchResultRow>> searchStudents(@RequestBody StudentSearchCriteria searchCriteria) {
    	Set<StudentSearchResultRow> students = studentService.searchStudents(searchCriteria);
        if(students.isEmpty()){
            return new ResponseEntity<Set<StudentSearchResultRow>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Set<StudentSearchResultRow>>(students, HttpStatus.OK);
    }

    
    //-------------------Retrieve Single Student--------------------------------------------------------
     
    @RequestMapping(value = "/students/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<StudentSearchResultRow> getStudent(@PathVariable("id") Long id) {
    	logger.debug("Fetching Student with id {}", id);
    	StudentSearchResultRow courseSearchResultRow = studentService.findById(id);
        if (courseSearchResultRow == null) {
        	logger.debug("Student with id {} not found", id);
            return new ResponseEntity<StudentSearchResultRow>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<StudentSearchResultRow>(courseSearchResultRow, HttpStatus.OK);
    }
 
     
     
    //-------------------Create a Student--------------------------------------------------------
     
    @RequestMapping(value = "/students", method = RequestMethod.POST)
    public ResponseEntity<Void> createStudent(@RequestBody UIStudent uiStudent, UriComponentsBuilder ucBuilder) {
    	logger.debug("Creating Student {} {}", uiStudent.getFirstName(), uiStudent.getLastName());
 
        Student course = studentService.saveStudent(uiStudent);
 
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/students/{id}").buildAndExpand(course.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }
 
    
     
    //------------------- Update a Student --------------------------------------------------------
     
    @RequestMapping(value = "/students/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Student> updateStudent(@PathVariable("id") Long id, @RequestBody UIStudent uiStudent) {
    	logger.debug("Updating Student {}", id);
    	
    	try {
    		Student currentStudent = studentService.updateStudent(uiStudent, id);
    		return new ResponseEntity<Student>(currentStudent, HttpStatus.OK);
    	}
    	catch (NotFoundException ex) {
    		return new ResponseEntity<Student>(HttpStatus.NOT_FOUND);
    	}
        
    }
}
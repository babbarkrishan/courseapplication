package com.babbarkrishan.university.controller;
 
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.babbarkrishan.university.bean.UIUniversity;
import com.babbarkrishan.university.bean.UniversitySearchResultRow;
import com.babbarkrishan.university.entity.University;
import com.babbarkrishan.university.exception.NotFoundException;
import com.babbarkrishan.university.service.CourseService;
import com.babbarkrishan.university.service.UniversityService;
 
@RestController
public class UniversityRestController {
	
	private static final Logger logger = LoggerFactory.getLogger(UniversityRestController.class);
 
    @Autowired
    UniversityService universityService;
    
    @Autowired
    CourseService courseService;
    
    //-------------------Retrieve All Universities--------------------------------------------------------
     
    @RequestMapping(value = "/universities", method = RequestMethod.GET)
    public ResponseEntity<Set<UniversitySearchResultRow>> listAllUniversities() {
    	Set<UniversitySearchResultRow> universities = universityService.findAllUniversities();
        if(universities.isEmpty()){
            return new ResponseEntity<Set<UniversitySearchResultRow>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Set<UniversitySearchResultRow>>(universities, HttpStatus.OK);
    } 
    
    //-------------------Retrieve Single University--------------------------------------------------------
     
    @RequestMapping(value = "/universities/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UniversitySearchResultRow> getUniversity(@PathVariable("id") Long id) {
    	logger.debug("Fetching University with id {}", id);
    	UniversitySearchResultRow universitySearchResultRow = universityService.findById(id);
        if (universitySearchResultRow == null) {
        	logger.debug("University with id {} not found", id);
            return new ResponseEntity<UniversitySearchResultRow>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<UniversitySearchResultRow>(universitySearchResultRow, HttpStatus.OK);
    }
 
     
     
    //-------------------Create a University--------------------------------------------------------
     
    @RequestMapping(value = "/universities", method = RequestMethod.POST)
    public ResponseEntity<Void> createUniversity(@RequestBody UIUniversity uiUniversity,    UriComponentsBuilder ucBuilder) {
    	logger.debug("Creating University {}", uiUniversity.getUniversityName());
    	
        if (universityService.isUniversityExist(uiUniversity)) {
        	logger.debug("A University with name {} already exist", uiUniversity.getUniversityName() );
            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }
 
        University university = universityService.saveUniversity(uiUniversity);
 
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/university/{id}").buildAndExpand(university.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }
    
    //------------------- Update a University --------------------------------------------------------
    
	@RequestMapping(value = "/universities/{id}", method = RequestMethod.PUT)
    public ResponseEntity<University> updateUniversity(@PathVariable("id") Long id, @RequestBody UIUniversity uiUniversity) {
    	logger.debug("Updating University {}", id);
 
    	try {
    		University university = universityService.updateUniversity(uiUniversity, id);
    		return new ResponseEntity<University>(university, HttpStatus.OK);
    	}
    	catch (NotFoundException ex) {
	    	return new ResponseEntity<University>(HttpStatus.NOT_FOUND);
    	}
    }
}
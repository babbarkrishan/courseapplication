package com.babbarkrishan.university.controller;
 
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.babbarkrishan.university.bean.CourseSearchCriteria;
import com.babbarkrishan.university.bean.CourseSearchResultRow;
import com.babbarkrishan.university.bean.UICourse;
import com.babbarkrishan.university.entity.Course;
import com.babbarkrishan.university.exception.NotFoundException;
import com.babbarkrishan.university.service.CourseService;
 
@RestController
public class CourseRestController {
	
	private static final Logger logger = LoggerFactory.getLogger(CourseRestController.class);
 
    @Autowired
    CourseService courseService; 
    
    //-------------------Retrieve All Courses--------------------------------------------------------
     
    @RequestMapping(value = "/courses", method = RequestMethod.GET)
    public ResponseEntity<Set<CourseSearchResultRow>> listAllCourses() {
        Set<CourseSearchResultRow> courses = courseService.findAllCourses();
        if(courses.isEmpty()){
            return new ResponseEntity<Set<CourseSearchResultRow>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Set<CourseSearchResultRow>>(courses, HttpStatus.OK);
    } 
    
    @RequestMapping(value = "/courses/search", method = RequestMethod.POST)
    public ResponseEntity<Set<CourseSearchResultRow>> searchCourses(@RequestBody CourseSearchCriteria searchCriteria) {
    	Set<CourseSearchResultRow> courses = courseService.searchCourses(searchCriteria);
        if(courses.isEmpty()){
            return new ResponseEntity<Set<CourseSearchResultRow>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Set<CourseSearchResultRow>>(courses, HttpStatus.OK);
    }

    
    //-------------------Retrieve Single Course--------------------------------------------------------
     
    @RequestMapping(value = "/courses/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CourseSearchResultRow> getCourse(@PathVariable("id") Long id) {
    	logger.debug("Fetching Course with id {}", id);
    	CourseSearchResultRow courseSearchResultRow = courseService.findById(id);
        if (courseSearchResultRow == null) {
        	logger.debug("Course with id {} not found", id);
            return new ResponseEntity<CourseSearchResultRow>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<CourseSearchResultRow>(courseSearchResultRow, HttpStatus.OK);
    }
 
     
     
    //-------------------Create a Course--------------------------------------------------------
     
    @RequestMapping(value = "/courses/", method = RequestMethod.POST)
    public ResponseEntity<Void> createCourse(@RequestBody UICourse uiCourse, UriComponentsBuilder ucBuilder) {
    	logger.debug("Creating Course {}", uiCourse.getCourseName());
 
        Course course = courseService.saveCourse(uiCourse);
 
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/courses/{id}").buildAndExpand(course.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }
 
    
     
    //------------------- Update a Course --------------------------------------------------------
     
    @RequestMapping(value = "/courses/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Course> updateCourse(@PathVariable("id") Long id, @RequestBody UICourse uiCourse) {
    	logger.debug("Updating Course {}", id);
    	
    	try {
    		Course currentCourse = courseService.updateCourse(uiCourse, id);
    		return new ResponseEntity<Course>(currentCourse, HttpStatus.OK);
    	}
    	catch (NotFoundException ex) {
    		return new ResponseEntity<Course>(HttpStatus.NOT_FOUND);
    	}
        
    }
}
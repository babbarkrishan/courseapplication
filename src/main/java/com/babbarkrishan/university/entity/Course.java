package com.babbarkrishan.university.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table (name = "course")
public class Course extends AbstractEntity {

	private static final long serialVersionUID = 1L;	
	
	private String courseName;
	private String duration;
	private String description;
	private String eligibility;
	private University university;
	
	public Course(){
		id = 0l;
	}
	
	public Course(Long id , String courseName, String duration, String description, String eligibility){
		this.id = id;
		this.courseName = courseName;
		this.duration = duration;
		this.description = description;
		this.eligibility = eligibility;
	}		

	@Column(name = "courseName")
	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	@Column(name = "duration")
	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	@Column(name = "description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "eligibility")
	public String getEligibility() {
		return eligibility;
	}

	public void setEligibility(String eligibility) {
		this.eligibility = eligibility;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "universityId", nullable = false)
	public University getUniversity() {
		return university;
	}

	public void setUniversity(University university) {
		this.university = university;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Course))
			return false;
		Course other = (Course) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "University [id =" + id + ", courseName=" + courseName + ", duration=" + duration
				+ ", description=" + description + ", eligibility =" + eligibility + "]";
	}	
}
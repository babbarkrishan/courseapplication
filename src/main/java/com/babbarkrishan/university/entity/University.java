package com.babbarkrishan.university.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table (name = "university")
public class University extends AbstractEntity {	
		
	private static final long serialVersionUID = 1L;
	private String universityName;	
	private String address;	
	private String email;	
	private String phoneNumber;	
	private Set<Course> courses = new HashSet<Course>(0);
	
	public University(){
		id = 0l;
	}
	
	public University(Long id, String universityName, String address, String email, String phoneNumber){
		this.id = id;
		this.universityName = universityName;
		this.address = address;
		this.email = email;
		this.phoneNumber = phoneNumber;
	}

	@Column(name = "universityName")
	public String getUniversityName() {
		return universityName;
	}

	public void setUniversityName(String universityName) {
		this.universityName = universityName;
	}

	@Column(name = "address")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Column(name = "email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "phoneNumber")
	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "university")
	@Cascade({CascadeType.ALL})
	public Set<Course> getCourses() {
		return this.courses;
	}

	public void setCourses(Set<Course> courses) {
		this.courses = courses;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof University))
			return false;
		University other = (University) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "University [id=" + id + ", universityName=" + universityName + ", address=" + address
				+ ", email=" + email + ", phoneNumber =" + phoneNumber + "]";
	}	
}
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html ng-app="myApp">
  <head>  
    <title>Course Application - Dashboard</title>  
    <style>
      .universityName.ng-valid {
          background-color: lightgreen;
      }
      .universityName.ng-dirty.ng-invalid-required {
          background-color: red;
      }
      .universityName.ng-dirty.ng-invalid-minlength {
          background-color: yellow;
      }

      .email.ng-valid {
          background-color: lightgreen;
      }
      .email.ng-dirty.ng-invalid-required {
          background-color: red;
      }
      .email.ng-dirty.ng-invalid-email {
          background-color: yellow;
      }

    </style>
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
     <link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
     
     <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.js"></script>
     <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular-route.js"></script>  
  </head>
  <body class="ng-cloak">
  	<div ng-view></div>
  	<script src="<c:url value='/static/js/app.js' />"></script>
  	<script src="<c:url value='/static/js/service/university_service.js' />"></script>
    <script src="<c:url value='/static/js/controller/university_controller.js' />"></script>
    
    <script src="<c:url value='/static/js/service/course_service.js' />"></script>
    <script src="<c:url value='/static/js/controller/course_controller.js' />"></script>
  </body>
</html>
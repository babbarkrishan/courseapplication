'use strict';

//var App = angular.module('myApp',[]);

var myApp = angular.module("myApp", ["ngRoute"]);
myApp.config(function($routeProvider) {	
    $routeProvider
    .when("/", {
        templateUrl : "static/templates/unversities.htm",
        controller: 'UniversityController'
    })
    .when("/courseList", {
        templateUrl : "static/templates/courses.htm",
        controller: 'CourseController'
    })
    .when("/green", {
        templateUrl : "green.htm"
    })
    .when("/blue", {
        templateUrl : "blue.htm"
    });
});

myApp.run(function($rootScope) {
	$rootScope.baseURL = "http://localhost:9090/CourseApplication/";
});
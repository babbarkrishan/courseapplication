'use strict';

angular.module('myApp').factory('UniversityService', ['$http', '$q', '$rootScope', function($http, $q, $rootScope){

    var REST_SERVICE_URI = $rootScope.baseURL + 'universities/';

    var factory = {
        fetchAllUniversities: fetchAllUniversities,
        createUniversity: createUniversity,
        updateUniversity:updateUniversity,
        deleteUniversity:deleteUniversity
    };

    return factory;

    function fetchAllUniversities() {
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URI)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while fetching Universities');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }

    function createUniversity(university) {
        var deferred = $q.defer();
        $http.post(REST_SERVICE_URI, university)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while creating University');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }


    function updateUniversity(university, id) {
        var deferred = $q.defer();
        $http.put(REST_SERVICE_URI+id, university)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while updating University');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }

    function deleteUniversity(id) {
        var deferred = $q.defer();
        $http.delete(REST_SERVICE_URI+id)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while deleting University');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }

}]);

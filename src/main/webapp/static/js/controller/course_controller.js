'use strict';

angular.module('myApp').controller('CourseController', ['$scope', 'CourseService', function($scope, CourseService) {
    var self = this;
    self.course={courseId:null,courseName:'',address:'',email:''};
    self.courses=[];

    self.submit = submit;
    self.edit = edit;
    self.remove = remove;
    self.reset = reset;


    fetchAllCourses();

    function fetchAllCourses(){
        CourseService.fetchAllCourses()
            .then(
            function(d) {
                self.courses = d;
            },
            function(errResponse){
                console.error('Error while fetching Courses');
            }
        );
    }

    function createCourse(course){
        CourseService.createCourse(course)
            .then(
            fetchAllCourses,
            function(errResponse){
                console.error('Error while creating Course');
            }
        );
    }

    function updateCourse(course, courseId){
        CourseService.updateCourse(course, courseId)
            .then(
            fetchAllCourses,
            function(errResponse){
                console.error('Error while updating Course');
            }
        );
    }

    function deleteCourse(courseId){
        CourseService.deleteCourse(courseId)
            .then(
            fetchAllCourses,
            function(errResponse){
                console.error('Error while deleting Course');
            }
        );
    }

    function submit() {
        if(self.course.courseId===null){
            console.log('Saving New Course', self.course);
            createCourse(self.course);
        }else{
            updateCourse(self.course, self.course.courseId);
            console.log('Course updated with courseId ', self.course.courseId);
        }
        reset();
    }

    function edit(courseId){
        console.log('courseId to be edited', courseId);
        for(var i = 0; i < self.courses.length; i++){
            if(self.courses[i].courseId === courseId) {
                self.course = angular.copy(self.courses[i]);
                break;
            }
        }
    }

    function remove(courseId){
        console.log('courseId to be deleted', courseId);
        if(self.course.courseId === courseId) {//clean form if the course to be deleted is shown there.
            reset();
        }
        deleteCourse(courseId);
    }


    function reset(){
        self.course={courseId:null,courseName:'',address:'',email:''};
        $scope.myForm.$setPristine(); //reset Form
    }

}]);

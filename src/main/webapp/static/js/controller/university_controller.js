'use strict';

angular.module('myApp').controller('UniversityController', ['$scope', 'UniversityService', function($scope, UniversityService) {
	
    var self = this;
    self.university={universityId:null,universityName:'',address:'',email:''};
    self.universities=[];

    self.submit = submit;
    self.edit = edit;
    self.remove = remove;
    self.reset = reset;


    fetchAllUniversities();

    function fetchAllUniversities(){
        UniversityService.fetchAllUniversities()
            .then(
            function(d) {
                self.universities = d;
            },
            function(errResponse){
                console.error('Error while fetching Universities');
            }
        );
    }

    function createUniversity(university){
        UniversityService.createUniversity(university)
            .then(
            fetchAllUniversities,
            function(errResponse){
                console.error('Error while creating University');
            }
        );
    }

    function updateUniversity(university, universityId){
        UniversityService.updateUniversity(university, universityId)
            .then(
            fetchAllUniversities,
            function(errResponse){
                console.error('Error while updating University');
            }
        );
    }

    function deleteUniversity(universityId){
        UniversityService.deleteUniversity(universityId)
            .then(
            fetchAllUniversities,
            function(errResponse){
                console.error('Error while deleting University');
            }
        );
    }

    function submit() {
        if(self.university.universityId===null){
            console.log('Saving New University', self.university);
            createUniversity(self.university);
        }else{
            updateUniversity(self.university, self.university.universityId);
            console.log('University updated with universityId ', self.university.universityId);
        }
        reset();
    }

    function edit(universityId){
        console.log('universityId to be edited', universityId);
        for(var i = 0; i < self.universities.length; i++){
            if(self.universities[i].universityId === universityId) {
                self.university = angular.copy(self.universities[i]);
                break;
            }
        }
    }

    function remove(universityId){
        console.log('universityId to be deleted', universityId);
        if(self.university.universityId === universityId) {//clean form if the university to be deleted is shown there.
            reset();
        }
        deleteUniversity(universityId);
    }


    function reset(){
        self.university={universityId:null,universityName:'',address:'',email:''};
        $scope.myForm.$setPristine(); //reset Form
    }

}]);
